﻿#pragma once

#include <string>
using namespace std;

bool arduino_parse(const string& srcFile, const string& boardFile, string& jsonOut);