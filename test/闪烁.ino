/**
 * Copyright(C), 2016-2038, https://www.mylecon.com/
 * FileName: MyProject.ino
 * Author: XBlockly
 * LastModified_at: 2018/5/5 ����9:03:38
 */

#include <HQRSystem.h>
#include <XIRTracking.h>
#include <XUltrasonic.h>
#include <XDualDCMotor.h>
#include <HQRCarDriver.h>
#include <XServo.h>
#include <XAdapter.h>
#include <XButton.h>
#include <XSoundSensor.h>
#include <XBuzzer.h>
#include <HQRAudioPlayer.h>
#include <XRGBLed.h>
#include <HQRLightShow.h>

float x;
float i;

XIRTracking MyXIRTracking_1;
XUltrasonic MyXUltrasonic_2;
XDualDCMotor MyXDualDCMotor_DDM;
HQRCarDriver MyHQRCarDriver_DDM;
XServo MyXServo_SVO;
XAdapter MyXAdapter_3;
XButton MyXButton_BTN;
XSoundSensor MyXSoundSensor_SND;
XBuzzer MyXBuzzer_BUZ;
HQRAudioPlayer MyHQRAudioPlayer_BUZ;
XRGBLed MyXRGBLed_RGB;
HQRLightShow MyHQRLightShow_RGB;

uint32_t colorRgb(int r, int g, int b) {
  uint32_t color = 0x00;
  color |= ((uint32_t)b&0xff);
  color |= (((uint32_t)g<<8)&0xff00);
  color |= (((uint32_t)r<<16)&0xff0000);
  return color;
}

// �����ù���...
void A() {
  MyHQRAudioPlayer_BUZ.setNoteParameter(0.8);
  i = 1.02;
  MyHQRAudioPlayer_BUZ.stop();
  MyHQRAudioPlayer_BUZ.playMusic(4,1);
  MyHQRLightShow_RGB.showColor(0,colorRgb(255, 0, 0));
  MyHQRLightShow_RGB.showBreath(0,1,colorRgb(255, 0, 0));
  MyHQRLightShow_RGB.showMeteor(1,colorRgb(255, 0, 0));
  MyHQRLightShow_RGB.clear(0);
}


void setup() {
  MyXIRTracking_1.setup("IRT3320", "1");
  MyXUltrasonic_2.setup("ULS3600", "2");
  MyXDualDCMotor_DDM.setup("DDM");
  MyHQRCarDriver_DDM.setup(&MyXDualDCMotor_DDM);
  MyXServo_SVO.setup("SVO");
  MyXAdapter_3.setup("Adapter", "3");
  MyXButton_BTN.setup("BTN");
  MyXSoundSensor_SND.setup("SND");
  MyXBuzzer_BUZ.setup("BUZ");
  MyHQRAudioPlayer_BUZ.setup(&MyXBuzzer_BUZ);
  MyXRGBLed_RGB.setup("RGB");
  MyHQRLightShow_RGB.setup(&MyXRGBLed_RGB);

  HQRSystem.setBaudrate(9600);

}

void loop() {
  MyHQRCarDriver_DDM.turn(0, 50, 15);
  MyHQRCarDriver_DDM.autoLineTracking(&MyXIRTracking_1,0);
  MyHQRCarDriver_DDM.autoObstacleAvoidance(&MyXUltrasonic_2,1);
  MyDualDCMotor_DDM.setMotorSpeed(0,10);
  MyDualDCMotor_DDM.stopMotor(0);
  MyXServo_SVO.setAngle(0);
  HQRSystem.watchdogEnable(0.5);
  HQRSystem.watchdogDisable();
  while(!(3==MyXIRTracking_1.getStatus()));
  if ((0==MyXIRTracking_1.getStatus())) {
    delay(1000);
    x = 123.01;
    while(!(3==MyXIRTracking_1.getStatus()));
    HQRSystem.printInteger(123, 2, true);
    HQRSystem.reboot();
    while(!MyXButton_BTN.isKnocked());
  } else {
    while(MyXSoundSensor_SND.getVolume()<=10);
  }
  while(MyXUltrasonic_2.getDistance()<=10);

  MyXAdapter_3.analogRead(1);

  HQRSystem.getSN();

  MyHQRCarDriver_DDM.loop();
  MyHQRAudioPlayer_BUZ.loop();
  MyHQRLightShow_RGB.loop();
}